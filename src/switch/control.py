import RPi.GPIO as GPIO
import time


A = 2
B = 3
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(A, GPIO.OUT) # V1 control signal
GPIO.setup(B, GPIO.OUT) # V2 control signal



if __name__ == '__main__':
    f = 4.8e3 # frequency = 4kHz
    T = 1/f
    delay = T/4
    while(1):
        GPIO.output(A, GPIO.LOW)
        GPIO.output(B, GPIO.LOW)
        time.sleep(delay)
        GPIO.output(A, GPIO.LOW)
        GPIO.output(B, GPIO.HIGH)
        time.sleep(delay)
        GPIO.output(A, GPIO.HIGH)
        GPIO.output(B, GPIO.HIGH)
        time.sleep(delay)
        GPIO.output(A, GPIO.HIGH)
        GPIO.output(B, GPIO.LOW)
        time.sleep(delay)
