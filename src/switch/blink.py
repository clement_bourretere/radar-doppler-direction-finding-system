import RPi.GPIO as GPIO
import time

LED = 21

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
try :
    GPIO.setup(LED, GPIO.OUT)
except:
    print("GPIO's setup is already done.")

if __name__ == "__main__":
    delay = 0.05
    wait = 2
    while True:
        GPIO.output(LED, GPIO.LOW)
        time.sleep(wait)
        GPIO.output(LED, GPIO.HIGH)
        time.sleep(delay)
        GPIO.output(LED, GPIO.LOW)
        time.sleep(delay)
        GPIO.output(LED, GPIO.HIGH)
        time.sleep(delay)

