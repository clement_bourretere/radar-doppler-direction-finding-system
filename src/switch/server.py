from socketserver import BaseRequestHandler
import os
from http.server import BaseHTTPRequestHandler, HTTPServer
import subprocess
import socket
import signal
from contextlib import contextmanager


host_name = "localhost"
host_port = 8000
UDP_IP = "127.0.0.1"
UDP_PORT = 5005


class TimeoutException(Exception): pass

@contextmanager
def time_limit(seconds):
    def signal_handler(signum, frame):
        raise TimeoutException("Timed out!")
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)

class MyServer(BaseHTTPRequestHandler):

    def do_HEAD(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def _redirect(self, path):
        self.send_response(303)
        self.send_header('Content-type', 'text/html')
        self.send_header('Location', path)
        self.end_headers()

    def log_message(self, format, *args):
        return

    def do_GET(self, _AOA=""):
        html = """ """
        file = open(os.getcwd()+"/html.txt", "r")
        lines = file.readlines()
        for l in lines:
            # AOA
            if l == '                                                                                                <span style="color:#000000;">AOA</span>\n':

                html+='\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span style="color:#000000;">AOA = {} °</span>\n'.format(_AOA)

            # Switch
            elif l == '                                                                                                    <span class="dot_gray" name="Switch"></span>\n':
                if RF_stat == 'Off':
                    html += l

                if RF_stat == 'On':
                   html += '                                                                                                    <span class="dot_green" name="Switch"></span>\n'

            # UDP
            elif l == '                                                                                                    <span class="dot_gray" name="UDP"></span>\n':
                if UDP_stat == 'Off':
                    html += l
                if UDP_stat == 'On':
                    html += '                                                                                                    <span class="dot_green" name="UDP"></span>\n'

            elif l == '        -webkit-transform:rotate(90deg);\n':
                html += '        -webkit-transform:rotate({}deg);\n'.format(_AOA)
            elif l == '        -moz-transform: rotate(90deg);\n':
                html += '        -moz-transform: rotate({}deg);\n'.format(_AOA)
            elif l == '        -ms-transform: rotate(90deg);\n':
                html += '        -ms-transform: rotate({}deg);\n'.format(_AOA)
            elif l == '        -o-transform: rotate(90deg);\n':
                html += '        -o-transform: rotate({}deg);\n'.format(_AOA)
            elif l == '        transform: rotate(90deg);\n':
                html += '        transform: rotate({}deg);\n'.format(_AOA)  
            else :
                html += l

        file.close()
        self.do_HEAD()
        self.wfile.write(html.encode("utf-8"))


    def do_POST(self):
        global RF_stat
        global UDP_stat
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length).decode("utf-8")
        post_data = post_data.split("=")[1]
        #RF Switch
        if post_data == "RF_On": 
            print("\033[93mNotice: \033[92mRF Switch is On\033[00m")
            RF_stat = "On"
            p = subprocess.Popen(['python', 'control.py'])
            q = subprocess.Popen(['python', 'blink.py'])
        if post_data == "RF_Off":
            print("\033[93mNotice: \033[91mRF Switch is Off\033[00m")
            RF_stat = "Off"
            try:
                os.system("kill $(ps aux | grep control.py | grep -v grep | awk '{ print $2 }')")
                os.system("kill $(ps aux | grep blink.py | grep -v grep | awk '{ print $2 }')")
                print("\033[93mConfirmation of the end of switching\033[00m")
            except:
                print("\033[91m/!\  The switching script is not terminated /!\\033[00m")
        #UDP 


        if post_data == "UDP_On": 
            print("\033[93mNotice: \033[92mUDP Communication is On\033[00m")
            UDP_stat = "On"
        if post_data == "UDP_Off": 
            print("\033[93mNotice: \033[91mUDP Communication is Off\033[00m") 
            UDP_stat = "Off"
            



        
        if post_data !="RF_On" and post_data !="RF_Off" and post_data !="UDP_On" and post_data !="UDP_Off":
            if UDP_stat == "On":
                try:
                    with time_limit(10):
                        sock = socket.socket(socket.AF_INET, # Internet
                                    socket.SOCK_DGRAM) # UDP
                        sock.bind((UDP_IP, UDP_PORT))
                        data, addr = sock.recvfrom(1024)
                        data = int.from_bytes(data, byteorder='big')
                        print("received message: %s" % data)
                        global _AOA
                        _AOA = data
                except TimeoutException as e:
                    print("\033[93mTimed Out ! No UDP Connexion :/\033[00m")
            else: 
                print("\033[93mYou must enable the UDP connection\033[00m")

        self.do_GET(_AOA)
        self._redirect('/')  # Redirect back to the root url


if __name__ == '__main__':
    http_server = HTTPServer((host_name, host_port), MyServer)
    print('Server Starts - {}:{}'.format(host_name, host_port))
    RF_stat = "Off"
    UDP_stat = "Off"
    _AOA = 42
    try:
        http_server.serve_forever()
    except KeyboardInterrupt:
        http_server.server_close()
        print("\n\tKeyboardInterrupt. Server closed.\n")
        
