options:
  parameters:
    author: ''
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: ''
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: qt_gui
    hier_block_src_path: '.:'
    id: top_block
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: prompt
    sizing_mode: fixed
    thread_safe_setters: ''
    title: ''
    window_size: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [11, 8]
    rotation: 0
    state: enabled

blocks:
- name: NFFT
  id: variable
  parameters:
    comment: ''
    value: '1024'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [11, 374]
    rotation: 0
    state: enabled
- name: carrier_freq
  id: variable
  parameters:
    comment: ''
    value: 2.3e9
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 212.0]
    rotation: 0
    state: enabled
- name: decimation
  id: variable
  parameters:
    comment: ''
    value: '10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [11, 60]
    rotation: 0
    state: enabled
- name: rotation_rate
  id: variable
  parameters:
    comment: ''
    value: 10e3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [12, 269]
    rotation: 0
    state: enabled
- name: samp_rate
  id: variable
  parameters:
    comment: ''
    value: 250k
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [9, 112]
    rotation: 0
    state: enabled
- name: signalThreshold
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: ''
    min_len: '200'
    orient: Qt.Horizontal
    rangeType: float
    start: '-100'
    step: '1'
    stop: '0'
    value: '-40'
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [759, 38]
    rotation: 0
    state: true
- name: skip_heads
  id: variable
  parameters:
    comment: ''
    value: '200'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [12, 322]
    rotation: 0
    state: enabled
- name: taps
  id: variable_low_pass_filter_taps
  parameters:
    beta: '6.76'
    comment: ''
    cutoff_freq: 2.5e3
    gain: '1'
    samp_rate: samp_rate
    width: 25e3
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [10, 428]
    rotation: 0
    state: true
- name: tuning_freq
  id: variable
  parameters:
    comment: ''
    value: 2.3e9
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [11, 165]
    rotation: 0
    state: enabled
- name: analog_agc_xx_0
  id: analog_agc_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: '1.0'
    max_gain: '100'
    maxoutbuf: '0'
    minoutbuf: '0'
    rate: 1e-4
    reference: '1.0'
    type: complex
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1181, 333]
    rotation: 180
    state: true
- name: analog_pwr_squelch_xx_0
  id: analog_pwr_squelch_xx
  parameters:
    affinity: ''
    alias: ''
    alpha: 100e-6
    comment: ''
    gate: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    ramp: '0'
    threshold: '-40'
    type: complex
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [375, 27]
    rotation: 0
    state: true
- name: analog_quadrature_demod_cf_0
  id: analog_quadrature_demod_cf
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: 800e-3
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [677, 212]
    rotation: 0
    state: true
- name: analog_sig_source_x_0
  id: analog_sig_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: 500e-3
    comment: ''
    freq: -4.8e3
    maxoutbuf: '0'
    minoutbuf: '0'
    offset: '0'
    phase: '0'
    samp_rate: 25e3
    type: complex
    waveform: analog.GR_COS_WAVE
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1146, 553]
    rotation: 0
    state: true
- name: band_pass_filter_0
  id: band_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: ''
    decim: '1'
    gain: '1'
    high_cutoff_freq: 5.12695e3
    interp: '1'
    low_cutoff_freq: 4.63867e3
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: samp_rate
    type: fir_filter_ccf
    width: 1.2207e3
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1152, 156.0]
    rotation: 0
    state: true
- name: blocks_complex_to_arg_0
  id: blocks_complex_to_arg
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [523, 433]
    rotation: 180
    state: true
- name: blocks_complex_to_mag_0
  id: blocks_complex_to_mag
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1018, 513]
    rotation: 180
    state: true
- name: blocks_divide_xx_0
  id: blocks_divide_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [724, 416]
    rotation: 180
    state: true
- name: blocks_float_to_complex_0
  id: blocks_float_to_complex
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [909, 212]
    rotation: 0
    state: true
- name: blocks_float_to_complex_1
  id: blocks_float_to_complex
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [844, 513]
    rotation: 180
    state: true
- name: blocks_multiply_xx_0
  id: blocks_multiply_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1243, 447]
    rotation: 180
    state: true
- name: blocks_udp_sink_0
  id: blocks_udp_sink
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    eof: 'True'
    ipaddr: 127.0.0.1
    port: '36563'
    psize: '1472'
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [240, 404.0]
    rotation: 180
    state: true
- name: freq_xlating_fir_filter_xxx_0
  id: freq_xlating_fir_filter_xxx
  parameters:
    affinity: ''
    alias: ''
    center_freq: 9e3
    comment: ''
    decim: decimation
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: samp_rate
    taps: taps
    type: ccc
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [425, 191]
    rotation: 0
    state: true
- name: osmosdr_source_0
  id: osmosdr_source
  parameters:
    affinity: ''
    alias: ''
    ant0: ''
    ant1: ''
    ant10: ''
    ant11: ''
    ant12: ''
    ant13: ''
    ant14: ''
    ant15: ''
    ant16: ''
    ant17: ''
    ant18: ''
    ant19: ''
    ant2: ''
    ant20: ''
    ant21: ''
    ant22: ''
    ant23: ''
    ant24: ''
    ant25: ''
    ant26: ''
    ant27: ''
    ant28: ''
    ant29: ''
    ant3: ''
    ant30: ''
    ant31: ''
    ant4: ''
    ant5: ''
    ant6: ''
    ant7: ''
    ant8: ''
    ant9: ''
    args: '0x100000663'
    bb_gain0: '20'
    bb_gain1: '20'
    bb_gain10: '20'
    bb_gain11: '20'
    bb_gain12: '20'
    bb_gain13: '20'
    bb_gain14: '20'
    bb_gain15: '20'
    bb_gain16: '20'
    bb_gain17: '20'
    bb_gain18: '20'
    bb_gain19: '20'
    bb_gain2: '20'
    bb_gain20: '20'
    bb_gain21: '20'
    bb_gain22: '20'
    bb_gain23: '20'
    bb_gain24: '20'
    bb_gain25: '20'
    bb_gain26: '20'
    bb_gain27: '20'
    bb_gain28: '20'
    bb_gain29: '20'
    bb_gain3: '20'
    bb_gain30: '20'
    bb_gain31: '20'
    bb_gain4: '20'
    bb_gain5: '20'
    bb_gain6: '20'
    bb_gain7: '20'
    bb_gain8: '20'
    bb_gain9: '20'
    bw0: 5e6
    bw1: '0'
    bw10: '0'
    bw11: '0'
    bw12: '0'
    bw13: '0'
    bw14: '0'
    bw15: '0'
    bw16: '0'
    bw17: '0'
    bw18: '0'
    bw19: '0'
    bw2: '0'
    bw20: '0'
    bw21: '0'
    bw22: '0'
    bw23: '0'
    bw24: '0'
    bw25: '0'
    bw26: '0'
    bw27: '0'
    bw28: '0'
    bw29: '0'
    bw3: '0'
    bw30: '0'
    bw31: '0'
    bw4: '0'
    bw5: '0'
    bw6: '0'
    bw7: '0'
    bw8: '0'
    bw9: '0'
    clock_source0: ''
    clock_source1: ''
    clock_source2: ''
    clock_source3: ''
    clock_source4: ''
    clock_source5: ''
    clock_source6: ''
    clock_source7: ''
    comment: ''
    corr0: '0'
    corr1: '0'
    corr10: '0'
    corr11: '0'
    corr12: '0'
    corr13: '0'
    corr14: '0'
    corr15: '0'
    corr16: '0'
    corr17: '0'
    corr18: '0'
    corr19: '0'
    corr2: '0'
    corr20: '0'
    corr21: '0'
    corr22: '0'
    corr23: '0'
    corr24: '0'
    corr25: '0'
    corr26: '0'
    corr27: '0'
    corr28: '0'
    corr29: '0'
    corr3: '0'
    corr30: '0'
    corr31: '0'
    corr4: '0'
    corr5: '0'
    corr6: '0'
    corr7: '0'
    corr8: '0'
    corr9: '0'
    dc_offset_mode0: '0'
    dc_offset_mode1: '0'
    dc_offset_mode10: '0'
    dc_offset_mode11: '0'
    dc_offset_mode12: '0'
    dc_offset_mode13: '0'
    dc_offset_mode14: '0'
    dc_offset_mode15: '0'
    dc_offset_mode16: '0'
    dc_offset_mode17: '0'
    dc_offset_mode18: '0'
    dc_offset_mode19: '0'
    dc_offset_mode2: '0'
    dc_offset_mode20: '0'
    dc_offset_mode21: '0'
    dc_offset_mode22: '0'
    dc_offset_mode23: '0'
    dc_offset_mode24: '0'
    dc_offset_mode25: '0'
    dc_offset_mode26: '0'
    dc_offset_mode27: '0'
    dc_offset_mode28: '0'
    dc_offset_mode29: '0'
    dc_offset_mode3: '0'
    dc_offset_mode30: '0'
    dc_offset_mode31: '0'
    dc_offset_mode4: '0'
    dc_offset_mode5: '0'
    dc_offset_mode6: '0'
    dc_offset_mode7: '0'
    dc_offset_mode8: '0'
    dc_offset_mode9: '0'
    freq0: 150e6
    freq1: 100e6
    freq10: 100e6
    freq11: 100e6
    freq12: 100e6
    freq13: 100e6
    freq14: 100e6
    freq15: 100e6
    freq16: 100e6
    freq17: 100e6
    freq18: 100e6
    freq19: 100e6
    freq2: 100e6
    freq20: 100e6
    freq21: 100e6
    freq22: 100e6
    freq23: 100e6
    freq24: 100e6
    freq25: 100e6
    freq26: 100e6
    freq27: 100e6
    freq28: 100e6
    freq29: 100e6
    freq3: 100e6
    freq30: 100e6
    freq31: 100e6
    freq4: 100e6
    freq5: 100e6
    freq6: 100e6
    freq7: 100e6
    freq8: 100e6
    freq9: 100e6
    gain0: '55'
    gain1: '10'
    gain10: '10'
    gain11: '10'
    gain12: '10'
    gain13: '10'
    gain14: '10'
    gain15: '10'
    gain16: '10'
    gain17: '10'
    gain18: '10'
    gain19: '10'
    gain2: '10'
    gain20: '10'
    gain21: '10'
    gain22: '10'
    gain23: '10'
    gain24: '10'
    gain25: '10'
    gain26: '10'
    gain27: '10'
    gain28: '10'
    gain29: '10'
    gain3: '10'
    gain30: '10'
    gain31: '10'
    gain4: '10'
    gain5: '10'
    gain6: '10'
    gain7: '10'
    gain8: '10'
    gain9: '10'
    gain_mode0: 'False'
    gain_mode1: 'False'
    gain_mode10: 'False'
    gain_mode11: 'False'
    gain_mode12: 'False'
    gain_mode13: 'False'
    gain_mode14: 'False'
    gain_mode15: 'False'
    gain_mode16: 'False'
    gain_mode17: 'False'
    gain_mode18: 'False'
    gain_mode19: 'False'
    gain_mode2: 'False'
    gain_mode20: 'False'
    gain_mode21: 'False'
    gain_mode22: 'False'
    gain_mode23: 'False'
    gain_mode24: 'False'
    gain_mode25: 'False'
    gain_mode26: 'False'
    gain_mode27: 'False'
    gain_mode28: 'False'
    gain_mode29: 'False'
    gain_mode3: 'False'
    gain_mode30: 'False'
    gain_mode31: 'False'
    gain_mode4: 'False'
    gain_mode5: 'False'
    gain_mode6: 'False'
    gain_mode7: 'False'
    gain_mode8: 'False'
    gain_mode9: 'False'
    if_gain0: '20'
    if_gain1: '20'
    if_gain10: '20'
    if_gain11: '20'
    if_gain12: '20'
    if_gain13: '20'
    if_gain14: '20'
    if_gain15: '20'
    if_gain16: '20'
    if_gain17: '20'
    if_gain18: '20'
    if_gain19: '20'
    if_gain2: '20'
    if_gain20: '20'
    if_gain21: '20'
    if_gain22: '20'
    if_gain23: '20'
    if_gain24: '20'
    if_gain25: '20'
    if_gain26: '20'
    if_gain27: '20'
    if_gain28: '20'
    if_gain29: '20'
    if_gain3: '20'
    if_gain30: '20'
    if_gain31: '20'
    if_gain4: '20'
    if_gain5: '20'
    if_gain6: '20'
    if_gain7: '20'
    if_gain8: '20'
    if_gain9: '20'
    iq_balance_mode0: '0'
    iq_balance_mode1: '0'
    iq_balance_mode10: '0'
    iq_balance_mode11: '0'
    iq_balance_mode12: '0'
    iq_balance_mode13: '0'
    iq_balance_mode14: '0'
    iq_balance_mode15: '0'
    iq_balance_mode16: '0'
    iq_balance_mode17: '0'
    iq_balance_mode18: '0'
    iq_balance_mode19: '0'
    iq_balance_mode2: '0'
    iq_balance_mode20: '0'
    iq_balance_mode21: '0'
    iq_balance_mode22: '0'
    iq_balance_mode23: '0'
    iq_balance_mode24: '0'
    iq_balance_mode25: '0'
    iq_balance_mode26: '0'
    iq_balance_mode27: '0'
    iq_balance_mode28: '0'
    iq_balance_mode29: '0'
    iq_balance_mode3: '0'
    iq_balance_mode30: '0'
    iq_balance_mode31: '0'
    iq_balance_mode4: '0'
    iq_balance_mode5: '0'
    iq_balance_mode6: '0'
    iq_balance_mode7: '0'
    iq_balance_mode8: '0'
    iq_balance_mode9: '0'
    maxoutbuf: '0'
    minoutbuf: '0'
    nchan: '1'
    num_mboards: '1'
    sample_rate: samp_rate
    sync: none
    time_source0: ''
    time_source1: ''
    time_source2: ''
    time_source3: ''
    time_source4: ''
    time_source5: ''
    time_source6: ''
    time_source7: ''
    type: fc32
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [122, 119]
    rotation: 0
    state: true
- name: qtgui_number_sink_0
  id: qtgui_number_sink
  parameters:
    affinity: ''
    alias: ''
    autoscale: 'False'
    avg: '0'
    color1: ("black", "black")
    color10: ("black", "black")
    color2: ("black", "black")
    color3: ("black", "black")
    color4: ("black", "black")
    color5: ("black", "black")
    color6: ("black", "black")
    color7: ("black", "black")
    color8: ("black", "black")
    color9: ("black", "black")
    comment: ''
    factor1: '1'
    factor10: '1'
    factor2: '1'
    factor3: '1'
    factor4: '1'
    factor5: '1'
    factor6: '1'
    factor7: '1'
    factor8: '1'
    factor9: '1'
    graph_type: qtgui.NUM_GRAPH_HORIZ
    gui_hint: ''
    label1: ''
    label10: ''
    label2: ''
    label3: ''
    label4: ''
    label5: ''
    label6: ''
    label7: ''
    label8: ''
    label9: ''
    max: '1'
    min: '-1'
    name: '""'
    nconnections: '1'
    type: float
    unit1: ''
    unit10: ''
    unit2: ''
    unit3: ''
    unit4: ''
    unit5: ''
    unit6: ''
    unit7: ''
    unit8: ''
    unit9: ''
    update_time: '0.10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [520, 524.0]
    rotation: 0
    state: true
- name: qtgui_sink_x_0
  id: qtgui_sink_x
  parameters:
    affinity: ''
    alias: ''
    bw: 250e3
    comment: ''
    fc: '0'
    fftsize: '1024'
    gui_hint: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    name: sink
    plotconst: 'True'
    plotfreq: 'True'
    plottime: 'True'
    plotwaterfall: 'True'
    rate: '10'
    showports: 'False'
    showrf: 'True'
    type: complex
    wintype: firdes.WIN_BLACKMAN_hARRIS
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [571, 35]
    rotation: 0
    state: true
- name: qtgui_sink_x_0_0
  id: qtgui_sink_x
  parameters:
    affinity: ''
    alias: ''
    bw: 25e3
    comment: ''
    fc: '0'
    fftsize: '1024'
    gui_hint: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    name: Sink
    plotconst: 'False'
    plotfreq: 'True'
    plottime: 'True'
    plotwaterfall: 'True'
    rate: '10'
    showports: 'False'
    showrf: 'True'
    type: complex
    wintype: firdes.WIN_BLACKMAN_hARRIS
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [859, 318]
    rotation: 180
    state: true
- name: qtgui_sink_x_0_1
  id: qtgui_sink_x
  parameters:
    affinity: ''
    alias: ''
    bw: 250e3
    comment: ''
    fc: '0'
    fftsize: '1024'
    gui_hint: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    name: sink
    plotconst: 'False'
    plotfreq: 'True'
    plottime: 'False'
    plotwaterfall: 'False'
    rate: '10'
    showports: 'False'
    showrf: 'True'
    type: float
    wintype: firdes.WIN_BLACKMAN_hARRIS
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [952, 68.0]
    rotation: 0
    state: true
- name: qtgui_sink_x_0_1_0
  id: qtgui_sink_x
  parameters:
    affinity: ''
    alias: ''
    bw: 250e3
    comment: ''
    fc: '0'
    fftsize: '1024'
    gui_hint: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    name: sink
    plotconst: 'False'
    plotfreq: 'True'
    plottime: 'True'
    plotwaterfall: 'False'
    rate: '10'
    showports: 'False'
    showrf: 'True'
    type: complex
    wintype: firdes.WIN_BLACKMAN_hARRIS
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1376, 372.0]
    rotation: 0
    state: true

connections:
- [analog_agc_xx_0, '0', qtgui_sink_x_0_0, '0']
- [analog_pwr_squelch_xx_0, '0', qtgui_sink_x_0, '0']
- [analog_quadrature_demod_cf_0, '0', blocks_float_to_complex_0, '0']
- [analog_quadrature_demod_cf_0, '0', qtgui_sink_x_0_1, '0']
- [analog_sig_source_x_0, '0', blocks_multiply_xx_0, '1']
- [band_pass_filter_0, '0', analog_agc_xx_0, '0']
- [band_pass_filter_0, '0', blocks_multiply_xx_0, '0']
- [blocks_complex_to_arg_0, '0', blocks_udp_sink_0, '0']
- [blocks_complex_to_arg_0, '0', qtgui_number_sink_0, '0']
- [blocks_complex_to_mag_0, '0', blocks_float_to_complex_1, '0']
- [blocks_divide_xx_0, '0', blocks_complex_to_arg_0, '0']
- [blocks_float_to_complex_0, '0', band_pass_filter_0, '0']
- [blocks_float_to_complex_1, '0', blocks_divide_xx_0, '1']
- [blocks_multiply_xx_0, '0', blocks_complex_to_mag_0, '0']
- [blocks_multiply_xx_0, '0', blocks_divide_xx_0, '0']
- [blocks_multiply_xx_0, '0', qtgui_sink_x_0_1_0, '0']
- [freq_xlating_fir_filter_xxx_0, '0', analog_quadrature_demod_cf_0, '0']
- [osmosdr_source_0, '0', analog_pwr_squelch_xx_0, '0']
- [osmosdr_source_0, '0', freq_xlating_fir_filter_xxx_0, '0']

metadata:
  file_format: 1
