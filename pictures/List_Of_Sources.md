# Liste des sources pour la figure de la chaine complète du projet:

• Software defined radio :
https://en.wikipedia.org/wiki/File:SDR_HackRF_one_PCB.jpg

• Raspberry Pi 4 :
https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up/1

• Laptop icon :
https://www.google.com/imgres?imgurl=https%3A%2F%2Fimg.freepik.com%2Ffree-vector%2Flaptop_53876-43918.jpg%3Fw%3D2000&imgrefurl=https%3A%2F%2Fwww.freepik.com%2Ffree-photos-vectors%2Flaptop-icon&tbnid=lrdq92O0jHYSgM&vet=10CD0QMyiBAWoXChMIuPDkuYzJ-QIVAAAAAB0AAAAAEAI..i&docid=bZm4XMzxg86b-M&w=2000&h=1999&q=laptop%20icon&client=safari&ved=0CD0QMyiBAWoXChMIuPDkuYzJ-QIVAAAAAB0AAAAAEAI

• Wifi :
https://commons.wikimedia.org/wiki/File:WiFi_Logo.svg